terraform {
  backend "s3" {
    bucket  = "ot-ec2"
    key     = "demo/terraform.tfstate"
    region  = "ap-northeast-1"
    encrypt = true
  }
}

